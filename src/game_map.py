import noise
import random
import logging

from atmospherics import GasMixture, Oxygen, Nitrogen


class Tile(object):
    _model = b'.'
    passable = True
    empty_volume = 3000
    gas_mixture = None
    exposed_to_void = True

    def __init__(self, value=0):
        if value > 150:
            self._model = b'#'
            self.passable = False
            self.empty_volume = 0
        if self.empty_volume:
            self.gas_mixture = GasMixture(self.empty_volume)

    @property
    def model(self):
        if self.gas_mixture and self.gas_mixture.pressure:
            max_pressure = max(self.gas_mixture.pressure, 1077)
            ratio = (self.gas_mixture.pressure / max_pressure * 10.) % 10
            if self.gas_mixture.pressure == max_pressure:
                return (None, None, b'!')
            else:
                return (None, None, bytes(str(int(ratio)), 'utf-8'))

        return (None, None, self._model)

    @model.setter
    def model(self, new_value):
        self._model = new_value

    def __str__(self):
        return self._model

    def __repr__(self):
        return '{}: model={}'.format(self.__class__.__name__, str(self._model, 'utf-8'))


class BreathableTile(Tile):
    _model = b'o'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.gas_mixture.add_gas(Oxygen, 27.7452 * 1.5)
        self.gas_mixture.add_gas(Nitrogen, 104.3748 * 1.5)


class VoidTile(Tile):
    _model = b' '
    passable = False
    empty_volume = 0


class Pos(object):
    def __init__(self, x_pos, y_pos):
        self.x_pos = x_pos
        self.y_pos = y_pos

    def __eq__(self, other_pos):
        return self.x_pos == other_pos.x_pos\
            and self.y_pos == other_pos.y_pos

    def __repr__(self):
        return str((self.x_pos, self.y_pos))


class GameMap(object):

    def __init__(self, height, width, seed=random.random()):
        self.height = height
        self.width = width
        self._generate(seed, 16, 5)

    def _generate(self, random_seed, frequency, octaves):
        logging.debug(
            'Generating new map using (random_seed=%d, frequency=%d, octaves=%d).',
            random_seed,
            frequency,
            octaves,
        )
        self._map = [
            [Tile(
                noise.pnoise3(
                    x / frequency * octaves,
                    y / frequency * octaves,
                    random_seed / frequency * octaves,
                    octaves=octaves
                ) * 127. + 128
           #     0
            ) for x in range(self.width)
            ] for y in range(self.height)
        ]

    @property
    def tiles(self):
        for y_pos, row in enumerate(self._map):
            for x_pos, tile in enumerate(row):
                yield (Pos(x_pos=x_pos, y_pos=y_pos), tile)

    @property
    def atmos_simulated_tiles(self):
        for y_pos, row in enumerate(self._map):
            for x_pos, tile in enumerate(row):
                if tile.gas_mixture:
                    pos = Pos(x_pos=x_pos, y_pos=y_pos)
                    yield (pos, tile, list(self.get_neighbours(pos)))

    def get_neighbours(self, tile_pos):
        offsets = (
            (0, -1),
            (1, 0),
            (0, 1),
            (-1, 0),
        )

        for x_offset, y_offset in offsets:
            pos = Pos(tile_pos.x_pos + x_offset, tile_pos.y_pos + y_offset)
            yield (pos, self[pos])

    @staticmethod
    def _equalize_gas(atmos_tile):
        tile_pos, tile, neighbour_tiles = atmos_tile
        changes = {
            'tile_pos': tile_pos,
            'flows': list(),
        }

        if tile.gas_mixture is None:
            return changes

        if tile.exposed_to_void:
            void_mixture = GasMixture(tile.empty_volume,
                                      tile.gas_mixture.temperature)
            top_tile = VoidTile()
            top_tile.gas_mixture = void_mixture
            neighbour_tiles.append((None, top_tile))

        changes['flows'] = tile.gas_mixture.equalize_gas(
            [(n_tile.gas_mixture, 3) for _, n_tile in neighbour_tiles],
        )
        return changes

    def _apply_mixture_removals(self, change):
        tile_pos = change['tile_pos']
        tile = self[tile_pos]
        flows = change['flows']
        total_flow = sum(flows)
        total_volume = min(
            tile.empty_volume / (1 + len([flow for flow in flows if flow])),
            total_flow * 0.04 * 1000
        )
        if total_volume < tile.empty_volume / 64:
            change['flows'] = [0 for _ in flows]
            return
        change['removed_mixture'] = tile.gas_mixture.remove_mixture(total_volume)

    def _apply_mixture_injections(self, change):
        if 'removed_mixture' not in change.keys():
            return
        flows = change['flows']
        total_flow = sum(flows)
        moved_mixture = change['removed_mixture']
        for (n_pos, _), flow in zip(self.get_neighbours(change['tile_pos']), flows):
            if flow == 0:
                continue
            ratio = flow / total_flow
            local_moved_mixture = moved_mixture * ratio
            self[n_pos].gas_mixture.inject_mixture(local_moved_mixture)

    def heartbeat(self):
        removals = list(filter(lambda change: sum(change['flows']) > 0,
                              [self._equalize_gas(tile) for tile in self.atmos_simulated_tiles]))
        for change in removals:
            self._apply_mixture_removals(change)
        injections = dict()
        for change in removals:
            flows = change['flows']
            source_tile = self[change['tile_pos']]
            total_flow = sum(flows)
            for (_, tile), flow in zip(self.get_neighbours(change['tile_pos']), flows):
                if flow == 0:
                    continue
                if tile not in injections.keys():
                    injections[tile] = list()
                ratio = flow / total_flow
                injected_mixture = change['removed_mixture'] * ratio
                injections[tile].append((source_tile, injected_mixture))

        for tile, injection_data in injections.items():
            total_volume = sum([mixture.volume for (_, mixture) in injection_data])
            for source_tile, injected_mixture in injection_data:
                volume_ratio = injected_mixture.volume / total_volume
                tile.gas_mixture.inject_mixture(injected_mixture * volume_ratio)
                source_tile.gas_mixture.inject_mixture(injected_mixture * (1 - volume_ratio))

    def __getitem__(self, pos):
        if pos.x_pos < 0 or pos.x_pos >= self.width\
                or pos.y_pos < 0 or pos.y_pos >= self.height:
            return VoidTile()
        return self._map[pos.y_pos][pos.x_pos]

    def __setitem__(self, pos, new_tile):
        if pos.x_pos < 0 or pos.x_pos >= self.width\
                or pos.y_pos < 0 or pos.y_pos >= self.height:
            raise IndexError
        self._map[pos.y_pos][pos.x_pos] = new_tile

__all__ = ['Tile', 'BreathableTile', 'VoidTile', 'Pos', 'GameMap']
