import urwid
import datetime
import logging
import shelve
import math
from game_map import *
from atmospherics import *


class GameEngine(object):
    _instance = None
    heartbeat_delay = 0.04
    palette = [
        ('body', 'black', 'light gray', 'standout'),
        ('header', 'white', 'dark red', 'bold'),
        ('screen edge', 'light blue', 'dark cyan'),
        ('main shadow', 'dark gray', 'black'),
        ('line', 'black', 'light gray', 'standout'),
        ('bg background', 'light gray', 'black'),
        ('bg 1', 'black', 'dark blue', 'standout'),
        ('bg 1 smooth', 'dark blue', 'black'),
        ('bg 2', 'black', 'dark cyan', 'standout'),
        ('bg 2 smooth', 'dark cyan', 'black'),
        ('button normal', 'light gray', 'dark blue', 'standout'),
        ('button select', 'white', 'dark green'),
        ('line', 'black', 'light gray', 'standout'),
        ('pg normal', 'white', 'black', 'standout'),
        ('pg complete', 'white', 'dark gray'),
        ('pg smooth', 'dark gray', 'black'),
    ]

    @staticmethod
    def get_instance():
        if '_instance' not in GameEngine.__dict__.keys()\
                or GameEngine._instance is None:
            raise RuntimeError('GameEngine manipulated before instanciation.')
        return GameEngine._instance

    @staticmethod
    def set_instance(new_instance):
        if '_instance' in GameEngine.__dict__.keys()\
                and GameEngine._instance is not None:
            raise TypeError('Only a single instance of GameEngine may exist at any given time')
        GameEngine._instance = new_instance

    def __init__(self, savegame_path='./savegame.cisav'):
        GameEngine.set_instance(self)
        self.savegame = shelve.open(savegame_path)
        self.game_map = 'game_map' in self.savegame and self.savegame['game_map']\
            or GameMap(17, 17, seed=680)

        from player import Player
        self.player = 'player' in self.savegame and self.savegame['player']\
            or Player(Pos(8, 8))
        self.tick_number = 'tick_number' in self.savegame and self.savegame['tick_number']\
            or 0
        self.last_heartbeat = datetime.datetime.now()
        self.ticks_since_last_heartbeat = 0

        from urwid_gui import MapWidget
        self.widget = MapWidget()
        self.oxygen_widget = urwid.ProgressBar('pg normal',
                                               'pg complete',
                                               0,
                                               100,
                                               )
        self.loop = urwid.MainLoop(
            urwid.Columns(
                [
                    urwid.Overlay(
                        self.widget,
                        urwid.SolidFill(' '),
                        align='left', width='pack',
                        valign='top', height='pack',
                    ),
                    urwid.Pile([
                        ('pack', urwid.Divider(div_char=u'\u2500')),
                        ('pack', self.oxygen_widget),
                        ('pack', urwid.Divider(div_char=u'\u2500')),
                    ]),
                ],
            ),
            self.palette,
            event_loop=urwid.AsyncioEventLoop(),
        )

    def _sync_save(self):
        self.savegame['game_map'] = self.game_map
        self.savegame['player'] = self.player
        self.savegame['tick_number'] = self.tick_number
        logging.info('Saving game')
        self.savegame.sync()

    def _heartbeat(self):
        self.ticks_since_last_heartbeat = 1
        self.tick_number = self.tick_number + math.floor(self.ticks_since_last_heartbeat)
        if self.ticks_since_last_heartbeat >= 1:
            self.player.heartbeat()
            self.oxygen_widget.set_completion(self.player.oxygen_level)
            self.game_map.heartbeat()
            self.widget.invalidate()
            self.last_heartbeat = datetime.datetime.now()
        self.loop.event_loop.alarm(self.heartbeat_delay,
                                    self._heartbeat)

    def run(self):
        self.loop.event_loop.alarm(self.heartbeat_delay,
                                   self._heartbeat)
        self._sync_save()
        self.last_heartbeat = datetime.datetime.now()
        self.ticks_since_last_heartbeat = 0
        try:
            self.loop.run()
        finally:
            self._sync_save()

__all__ = ['GameEngine']
