import urwid

from game_map import *
from main import GameEngine


class PlayerSightCanvas(urwid.Canvas):
    def __init__(self, *args, **kwargs):
        urwid.Canvas.__init__(self, *args, **kwargs)
        self.size = (GameEngine.get_instance().player.line_of_sight * 2 + 1,
                     GameEngine.get_instance().player.line_of_sight * 2 + 1)
        self.cursor = None

    def cols(self):
        return self.size[0]

    def rows(self):
        return self.size[1]

    def content(self, trim_left=0, trim_top=0, cols=None, rows=None, attr_map=None):
        if cols is None:
            cols = self.size[0]
        if rows is None:
            rows = self.size[1]
        for row_num in range(GameEngine.get_instance().player.line_of_sight, -GameEngine.get_instance().player.line_of_sight - 1, -1):
            row = list()
            for col_num in range(GameEngine.get_instance().player.line_of_sight, -GameEngine.get_instance().player.line_of_sight - 1, -1):
                if col_num == 0 and row_num == 0:
                    tile = (None, None, b'@')
                else:
                    map_x = GameEngine.get_instance().player.pos.x_pos - col_num
                    map_y = GameEngine.get_instance().player.pos.y_pos - row_num
                    tile = GameEngine.get_instance().game_map[Pos(map_x, map_y)].model
                row.append(tile)
            yield row


class MapWidget(urwid.Widget):
    _sizing = frozenset(['fixed'])
    _selectable = True

    def pack(self, *_, **__):
        return (GameEngine.get_instance().player.line_of_sight * 2 + 1, GameEngine.get_instance().player.line_of_sight * 2 + 1)

    def render(self, size, focus=False):
        return PlayerSightCanvas()

    def invalidate(self):
        self._invalidate()

    def keypress(self, size, key):
        if key == 'up':
            GameEngine.get_instance().player.move(0, -1)
            self._invalidate()
        elif key == 'down':
            GameEngine.get_instance().player.move(0, 1)
            self._invalidate()
        elif key == 'left':
            GameEngine.get_instance().player.move(-1, 0)
            self._invalidate()
        elif key == 'right':
            GameEngine.get_instance().player.move(1, 0)
            self._invalidate()
        elif key == '+':
            GameEngine.get_instance().heartbeat_delay = 0.00
        elif key == '-':
            GameEngine.get_instance().heartbeat_delay = 0.04
        return key

__all__ = ['PlayerSightCanvas', 'MapWidget']
