from game_engine import GameEngine
from atmospherics import *
from game_map import *

class Player(object):
    line_of_sight = 8
    oxygen_consumption = .02  # FIXME

    def __init__(self, start_pos):
        self.pos = start_pos
        self.oxygen_level = 100.

    def heartbeat(self):
        self.oxygen_level = self.oxygen_level - self.oxygen_consumption * GameEngine.get_instance().ticks_since_last_heartbeat
        exterior_mixture = GameEngine.get_instance().game_map[self.pos].gas_mixture
        if exterior_mixture is not None and exterior_mixture.gas_ratio(Oxygen) > .16:
            self.oxygen_level = 100

        if self.oxygen_level <= 0:  # FIXME
            raise Exception('Dead', GameEngine.get_instance().tick_number)

    def move(self, delta_x, delta_y):
        new_pos = Pos(self.pos.x_pos + delta_x, self.pos.y_pos + delta_y)
        if not GameEngine.get_instance().game_map[new_pos].passable:
            return False
        self.pos = new_pos
        return True

__all__ = ['Player']
